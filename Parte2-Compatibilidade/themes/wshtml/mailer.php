<?php

//Tratamento do POST
date_default_timezone_set('America/Sao_Paulo');
$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$nome = trim(ucwords($post['nome']));
$email = trim($post['email']);
$tel = trim($post['tel']);
$msg = trim($post['msg']);
$tipo = trim($post['tipo']);
$tipo_nome = trim($post['tipo_nome']);

require('mail/class.phpmailer.php');
require('mail/class.smtp.php');

// Inicia a classe PHPMailer
$mail = new PHPMailer();

//Define os dados do servidor SMTP
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPAuth = true; // Autenticação
$mail->SMTPSecure = 'tls';

//Define o usuário e senha do e-mail
$mail->Username = 'daniel.ale87@gmail.com';
$mail->Password = 'sum41rox';

//Define os dados técnicos da mensagem
$mail->CharSet = 'UTF-8'; // Charset da mensagem (opcional)
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
//Define o remetente
$mail->From = 'daniel.ale87@gmail.com';
$mail->FromName = 'Daniel';

//Define o destinatário
$mail->addReplyTo($email, $nome);
$mail->AddAddress('daniel.ale87@gmail.com', 'Daniel');

//Texto e Assunto
$mail->Subject = "Reflessione | $tipo_nome"; // Assunto da mensagem
$mail->Body = "<p><b>Nome:</b> $nome</p>" .
        '<p><b>Data:</b> ' . date('d/m/Y H:i:s', time()) . '</p>' .
        "<p><b>Email:</b> $email" .
        "<p><b>Telefone:</b> $tel" .
        "<p><b>Mensagem</b>: $msg</p>";

//Envio da Mensagem
$enviado = $mail->Send();

//Exibe uma mensagem de resultado e retorna o e-mail para o cliente
if ($enviado):
    
    $resposta = array('sta' => true);
    
    $mail->AddAddress($email, $nome);
    $mail->Subject = "Reflessione | $tipo_nome";
    
    switch ($tipo):
        case 0:
            $mail->Body = "<p>Olá, <b>$nome</b>!</p><br>" .
                "<p>Agradecemos imensamente o feedback! A sua satisfação é " .
                "essencial para nós!</p><br>" .
                "<p>Atenciosamente</p><br>" .
                "<p><b>Equipe Reflessione</b></p>" .
                "<p>(Favor não responder esse e-mail)</p>";
            break;
        case 1:
            $mail->Body = "<p>Olá, <b>$nome</b>!</p><br>" .
                "<p>Agradecemos o envio da sua dúvida! Retornaremos o contato " .
                "em breve com uma resposta!</p><br>" .
                "<p>Atenciosamente</p><br>" .
                "<p><b>Equipe Reflessione</b></p>" .
                "<p>(Favor não responder esse e-mail)</p>";
            break;
        case 2:
            $mail->Body = "<p>Olá, <b>$nome</b>!</p><br>" .
                "<p>Agradecemos a sua sugestão! Ela será analisada " .
                "e em breve retornaremos o contato com uma resposta!</p><br>" .
                "<p>Atenciosamente</p><br>" .
                "<p><b>Equipe Reflessione</b></p>" .
                "<p>(Favor não responder esse e-mail)</p>";
            break;
        default: 
            $mail->Body = "";
            break;
    endswitch;

    $mail->Send();
else:

    $resposta = array('sta' => false);
endif;

$mail->ClearAllRecipients();
$mail->ClearAttachments();

echo json_encode($resposta);
