<div class='modal'></div>
<section class='container'>
    <div class='content'>
        <header class='section-title'>
            <h2 class='font-large'><?php echo $pg_title; ?></h2>
            <p class='tagline'><?php echo $pg_desc; ?></p>
        </header>   

        <label class='mostrar_label'>
            Exibir
            <select class='mostrar'>
                <option value='5' selected>5</option>
                <option value='10'>10</option>
                <option value='20'>20</option>
                <option value='40'>40</option>
            </select>
        </label>

        <article id='imagens'></article>  

        <div class='modal_img'>
            <span class='fechar'>&times;</span>  
            <ul class='slider'>            
                <?php require('inc/slider_imagens.php'); ?>
            </ul>
        </div>

        <div class='service_modal'></div>
    </div>
    <div class='clear'></div>
</section>
<script src='<?php echo REQUIRE_PATH . '/js/pgw.js'; ?>'></script>
<script src='<?php echo REQUIRE_PATH . '/js/imagens.js'; ?>'></script>
<script src='<?php echo REQUIRE_PATH . '/js/img_slider.js'; ?>'></script>



