<?php
//Diretório de imagens upadas
$diretorio_video = './uploads/media/';

//Lê todos os arquivos dentro do diretório e
//os coloca dentro de um array por data
$datas_video = array();
foreach(scandir($diretorio_video) as $arquivo):
    if(is_file($diretorio_video . $arquivo) && $arquivo != 'Thumbs.db'):
        $data = date('Y-m-d H:i:s', filectime($diretorio_video . $arquivo));
        $data = new DateTime($data);       
        $video_ordenado[$arquivo] = verificaData($data, $datas_video);
        $datas_video[] = $video_ordenado[$arquivo];
    endif;
endforeach;

//Ordena o array por data decrescente
arsort($video_ordenado, SORT_STRING);

//Limita o array para 4 posições
$video_ordenado = array_slice($video_ordenado, 0, 3);

//Cria os slides com as imagens de acordo com o nome do arquivo
$texto_main_videos = "<article class='box box-small'>
            <div class='thumb'>
              <div class='video_play radius'></div>
              <img title='Conceito' alt='Conceito' class='radius'
                   src='$diretorio_video/main/Conceito.jpg'/>
            </div>
            <h4 class='txt-center media-title'>Conceito</h4>
          </article>" ;

foreach($video_ordenado as $key=>$value):
    $alt = explode('.' , $key);
    $src = $diretorio_video . 'thumb/' . $alt[0] . '.jpg';
    $thumb_alt = $diretorio_video . 'thumb/' . $alt[0] . '.jpg';
    $thumb = (file_exists($thumb_alt) ? $src : $diretorio_video . 
                                             'thumb/thumb.png');
    $texto_main_videos .= "<article class='box box-small'>
                <div class='thumb'>
                    <div class='video_play radius'></div>
                    <img title='$alt[0]' alt='$alt[0]'
                    src='$thumb' class='radius'/>
                </div>
                <h4 class='txt-center media-title'>$alt[0]</h4>
            </article>" ;
endforeach;          

echo utf8_encode($texto_main_videos);

function verificaData($data, $array)
{
    $dataf = $data->format('d/m/Y H:i:s');
    if(in_array($dataf, $array)):
        $data->modify('-1 second');
        verificaData($data, $array);
    endif;
    
    $dataf = $data->format('d/m/Y H:i:s');
    return $dataf;
}