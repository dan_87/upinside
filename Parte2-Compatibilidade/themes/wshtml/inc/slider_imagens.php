<?php
//Diretório de imagens upadas
$diretorio_img_slider = './uploads/img/galeria/';

//Lê todos os arquivos dentro do diretório e
//os coloca dentro de um array por data

$datas_slider = array();
foreach(scandir($diretorio_img_slider) as $arquivo):
    if(is_file($diretorio_img_slider . $arquivo) && $arquivo != 'Thumbs.db'):
        $data = date('Y-m-d H:i:s', filectime($diretorio_img_slider . $arquivo));
        $data = new DateTime($data);       
        $img_ordenado_slider[$arquivo] = verificaData($data, $datas_slider);
        $datas_slider[] = $img_ordenado_slider[$arquivo];
    endif;
endforeach;

//Limita o array para 6 indices e o ordena por data decrescente
arsort($img_ordenado_slider, SORT_STRING);

//Cria os slides com as imagens de acordo com o nome do arquivo
foreach($img_ordenado_slider as $key=>$value):
    $alt = explode('.' , $key);
    $src = $diretorio_img_slider . $key;
    $texto .= "<li>
                <img src='$src' alt='$alt[0]' title='$alt[0]'/>
              </li>" ;
endforeach;       

echo utf8_encode($texto);

function verificaData($data, $array)
{
    $dataf = $data->format('d/m/Y H:i:s');
    if(in_array($dataf, $array)):
        $data->modify('-1 second');
        verificaData($data, $array);
    endif;
    
    $dataf = $data->format('d/m/Y H:i:s');
    return $dataf;
}
           