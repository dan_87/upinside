<div class='container bg-blue-marine'>
    <div class='content txt-center font-medium 
         font-300 font-color-purple '>
        Compartilhe nas Redes Sociais
    </div>
    <div class='content ft-center box_social'>
        <div>
            <a class='btn btn_fb' href='<?php echo 'http://www.facebook.com/sharer.php?u=' .
                urlencode($pg_url);?>' onclick="return !window.open(this.href, 'Facebook',
                                'left=170, top=70, width=640, height=580')">
                Facebook
            </a>
        </div>        
        <div>
            <a class='btn btn_tw' href='<?php
               echo 'https://twitter.com/intent/tweet?url=' .
               urlencode($pg_url) . '&hashtags=reflessione%2Cbelezacomterapia&text=' .
               urlencode($pg_header . ' -');?>' onclick="return !window.open(this.href, 'Google+',
                                                        'left=170, top=70, width=640, height=450')">                             
                Twitter
            </a>
        </div>
        <div>
            <a class='btn btn_gp' href='<?php echo 'https://plus.google.com/share?url=' .
               urlencode($pg_url);
               ?>' onclick="return !window.open(this.href, 'Google+',
                                'left=170, top=70, width=640, height=580')">
                Google+
            </a>
        </div>
        <div>
            <a class='btn btn_lk' href='<?php echo 'https://www.linkedin.com/cws/share?url=' .
               urlencode($pg_url);
               ?>' onclick="return !window.open(this.href, 'LinkedIn',
                                'left=170, top=70, width=640, height=580')">
                LinkedIn
            </a>
        </div>
    </div>
</div>