<?php

//Diretório de imagens upadas
$diretorio_img = './uploads/img/galeria/';

//Lê todos os arquivos dentro do diretório e
//os coloca dentro de um array por data
foreach (scandir($diretorio_img) as $arquivo):
    if (is_file($diretorio_img . $arquivo) && $arquivo != 'Thumbs.db'):
        $img_ordenado[$arquivo] = date('d/m/Y H:i:s', filectime($diretorio_img . $arquivo));
    endif;
endforeach;

//Limita o array para 6 indices e o ordena por data decrescente
$img_ordenado = array_slice($img_ordenado, 0, 6);

//Obtém apenas os elementos da página clicada
arsort($img_ordenado, SORT_STRING);

//Cria os slides com as imagens de acordo com o nome do arquivo
foreach ($img_ordenado as $key => $value):
    $alt = explode('.', $key);
    $src = $diretorio_img . $key;
    $texto_main_imagens .= "<li>                 
                                <img src='$src' alt='$alt[0]'/>
                            </li>";
endforeach;

echo utf8_encode($texto_main_imagens);
?>