<section class='container'>  
    <div class='content'>
        <header class='section-title section-title-nomargin'>
            <h2>Ops! Página não encontrada!</h2>
            <p class='tagline'><?php echo $pg_desc; ?></p>
        </header> 
        <div class='clear'></div>
    </div>

    <article class='bg-gray-light not_found'>
        <div class='content'>
            <header class='section-title'>
                <h3>Opções de Páginas</h3>
                <p class='tagline'>Desejo retornar para &nbsp;<span class='icon icon-arrowdown'></span></p>
            </header>

            <article class='related_item box box-medium'>
                <img title='Retornar para a página Inicial' alt='Retornar para a página Inicial'
                     class='radius' src='<?php echo IMG . 'home.jpg'; ?>'/>
                <h4>
                    <a href='<?php echo HOME; ?>' title='Retornar para a página Inicial'>Início</a>
                </h4>
                <p class='tagline font-color-purple'>Saiba o que podemos proporcionar a você!</p>
            </article>

            <article class='related_item box box-medium'>
                <img title='Retornar para a página Sobre a Empresa' alt='Retornar para a página Sobre a Empresa'
                     class='radius' src='<?php echo IMG . 'sobre.jpg'; ?>'/>
                <h4>
                    <a href='sobre' title='Retornar para a página Sobre a Empresa'>Sobre</a>
                </h4>
                <p class='tagline font-color-purple'><?php echo $descricao['sobre']; ?></p>
            </article>

            <article class='related_item box box-medium'>
                <img title='Retornar para a página de Serviços' alt='Retornar para a página de Serviços'
                     class='radius' src='<?php echo IMG . 'servicos.jpg'; ?>'/>
                <h4>
                    <a href='servicos' title='Retornar para a página de Serviços'>Serviços</a>
                </h4>
                <p class='tagline font-color-purple'><?php echo $descricao['servicos']; ?></p>
            </article>

            <article class='related_item box box-medium'>
                <img title='Retornar para a galeria de Imagens' alt='Retornar para a galeria de Imagens'
                     class='radius' src='<?php echo IMG . 'imagens.jpg'; ?>'/>
                <h4>
                    <a href='imagens' title='Retornar para a galeria de Imagens'>Imagens</a>
                </h4>
                <p class='tagline font-color-purple'><?php echo $descricao['imagens']; ?></p>
            </article>

            <article class='related_item box box-medium'>
                <img title='Retornar para a galeria de Vídeos' alt='Retornar para a galeria de Vídeos'
                     class='radius' src='<?php echo IMG . 'videos.jpg'; ?>'/>
                <h4>
                    <a href='videos' title='Retornar para a galeria de Vídeos'>Vídeos</a>
                </h4>
                <p class='tagline font-color-purple'><?php echo $descricao['videos']; ?></p>
            </article>            

            <article class='related_item box box-medium'>
                <img title='Retornar para a página de Contato' alt='Retornar para a página de Contato'
                     class='radius' src='<?php echo IMG . 'contato.jpg'; ?>'/>
                <h4>
                    <a href='contato' title='Retornar para a página de Contato'>Contato</a>
                </h4>
                <p class='tagline font-color-purple'><?php echo $descricao['contato']; ?></p>
            </article>

            <div class='clear'></div>
        </div>
    </article>
</section>
<script src='<?php echo REQUIRE_PATH . '/js/404.js'; ?>'></script>