<script src='<?php echo REQUIRE_PATH . '/js/service.js'; ?>'></script>
<section class='container'>
    <div class='content'>
        <header class='section-title'>
            <h2 class='font-large'><?php echo $pg_title; ?></h2>
            <p class='tagline'><?php echo $pg_desc; ?></p>
        </header>   

        <article class='bg-gray-lightest box box-small service'>      
            <img src='<?php echo INCLUDE_PATH . '/img/iniziale_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Iniziale!</h3>                
            </div>
            <div class='modal_content'>
                <span class='fechar'>&times;</span>           
                <p class='detail txt-center'>O 'Iniziale!' é ideal para quem está 
                    em busca do primeiro passo para sua autenticidade. Análise 
                    visagista e terapêutica, adequação estética e muito mais!</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <ul class='disc'>
                    <li>Análise visagista e terapêutica para identificação
                        de perfil psicológico, comportamental e temperamental;</li>
                    <li>Adequação estética: corte de cabelo e coloração (ou procedimento
                        necessário), design de sobrancelhas, depilação facial e maquiagem;</li>
                    <li>Análise do antes e depois através de fotos;</li>
                    <li>Acompanhamento on line diário com todos os profissionais durante
                        todo o programa;</li>
                    <li>Material personalizado com a devolutiva dos testes aplicados e 
                        feedback de todo o processo.</li>
                </ul>  
                <p><b>Encontros:</b> 03 (aproximadamente).</p>               
            </div>
        </article>

        <article class='bg-gray-lightest box box-small service'>    
            <img src='<?php echo INCLUDE_PATH . '/img/essenziale_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Essenziale!</h3>                   
            </div>            
            <div class='modal_content'>
                <span class='fechar'>&times;</span>  
                <p class='detail txt-center'>Essencial é poder ficar linda todos os dias
                    de sua vida. Além da adequação de imagem, tenha uma aula de auto maquiagem visagista
                    e saiba exatamente quais cores, produtos e tipos de maquiagem são adequadas a você!</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <ul class='disc'>
                    <li>Análise visagista e terapêutica para identificação
                        de perfil psicológico, comportamental e temperamental;</li>
                    <li>Aula e orientação de auto maquiagem personalizada com professora visagista;</li>
                    <li>Adequação estética: corte de cabelo e coloração (ou procedimento
                        necessário), design de sobrancelhas, depilação facial e maquiagem;</li>
                    <li>Análise do antes e depois através de fotos;</li>
                    <li>Acompanhamento on line diário com todos os profissionais durante
                        todo o programa;</li>
                    <li>Material personalizado com a devolutiva dos testes aplicados e 
                        feedback de todo o processo.</li>
                </ul>  
                <p><b>Encontros:</b> 04 (aproximadamente).</p>                
            </div>    
        </article>

        <article class='bg-gray-lightest box box-small service'>      
            <img src='<?php echo INCLUDE_PATH . '/img/sensazionale_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Sensazionale!</h3>                
            </div>
            <div class='modal_content'>
                <span class='fechar'>&times;</span>       
                <p class='detail txt-center'>Poder ser você por dentro e por fora é
                    sensacional! Registre tudo isso de uma maneira especial e se redescubra através de um lindo
                    ensaio fotográfico profissional, que deixará registrado todo o processo de encontro com sua
                    verdadeira beleza!</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <ul class='disc'>
                    <li>Análise visagista e terapêutica para identificação
                        de perfil psicológico, comportamental e temperamental;</li>
                    <li>Aula e orientação de auto maquiagem personalizada com professora visagista;</li>
                    <li>Adequação estética: corte de cabelo e coloração (ou procedimento
                        necessário), design de sobrancelhas, depilação facial e maquiagem;</li>
                    <li>Ensaio fotográfico profissional;</li>
                    <li>Análise do antes e depois através de fotos;</li>
                    <li>Acompanhamento on line diário com todos os profissionais durante
                        todo o programa;</li>
                    <li>Material personalizado com a devolutiva dos testes aplicados e 
                        feedback de todo o processo.</li>
                </ul>  
                <p><b>Encontros:</b> 05 (aproximadamente).</p>                
            </div>  
        </article>

        <article class='bg-gray-lightest box box-small service'>      
            <img src='<?php echo INCLUDE_PATH . '/img/fenomenale_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Fenomenale!</h3>                
                <div class='modal_content'>                    
                    <span class='fechar'>&times;</span>      
                    <p class='detail txt-center'>Seja fenomenal da cabeça aos pés! Uma 
                        equipe especialmente preparada para te conduzir à descoberta da sua autenticidade, 
                        te deixando linda desde como você se veste, até a maneira de como você se vê!</p>
                    <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                    <ul class='disc'>
                        <li>Análise visagista e terapêutica para identificação
                            de perfil psicológico, comportamental e temperamental;</li>
                        <li>Aula e orientação de auto maquiagem personalizada com professora visagista;</li>
                        <li>Aula e orientação de estilo personalizada com consultora de image;</li>
                        <li>Ensaio fotográfico profissional;</li>
                        <li>Adequação estética: corte de cabelo e coloração (ou procedimento
                            necessário), design de sobrancelhas, depilação facial e maquiagem;</li>
                        <li>Acompanhamento on line diário com todos os profissionais durante
                            todo o programa;</li>
                        <li>Material personalizado com a devolutiva dos testes aplicados e 
                            feedback de todo o processo, incluindo um vídeo com seus melhores momentos.</li>
                    </ul>  
                    <p><b>Encontros:</b> 07 (aproximadamente).</p>                
                </div> 
        </article>

        <div class='clear'></div>

        <article class='bg-gray-lightest box box-small service'>      
            <img src='<?php echo INCLUDE_PATH . '/img/tranquillita_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Tranquillitá!</h3>               
            </div>
            <div class='modal_content'>               
                <span class='fechar'>&times;</span>     
                <p class='detail txt-center'>O SEU momento com todos os
                    cuidados necessários para te deixar ainda mais linda e tranquila!</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <ul class='disc'>
                    <li>Café da manhã e coffee break;</li>
                    <li>Massagem relaxante;</li>
                    <li>Banho de imersão terapêutica com cromoterapia;</li>
                    <li>Meditação, Reiki e Dinâmica terapêutica;</li>
                    <li>Manutenção estética (sobrancelha, corte, hidratação e finalização
                        com escova ou babyliss).</li>
                </ul>  
                <p><b>Duração:</b> De 04 a 05 horas (aproximadamente).</p>                
            </div>                   
        </article>

        <article class='bg-gray-lightest box box-small service'>      
            <img src='<?php echo INCLUDE_PATH . '/img/maternita_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Maternitá!</h3>                
            </div>
            <div class='modal_content'>                
                <span class='fechar'>&times;</span>     
                <p class='detail txt-center'>Um momento de cuidados especiais 
                    para a futura mamãe e de muito amor para o bebê!</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <ul class='disc'>
                    <li>Café da manhã, coffee break e almoço;</li>
                    <li>Refeição para até 01 acompanhante (consulte o cardápio
                        do almoço);</li>
                    <li>Massagem relaxante;</li>
                    <li>Banho de imersão terapêutica com cromoterapia;</li>
                    <li>Meditação, Reiki e Dinâmica terapêutica;</li>
                    <li>Embelezamento (sobrancelha, corte, hidratação e finalização
                        com escova ou babyliss e maquiagem);</li>
                    <li>Ensaio fotográfico profissional.</li>
                </ul>  
                <p><b>Duração:</b> 06 a 08 horas (aproximadamente).</p>                
            </div>                   
        </article>

        <article class='bg-gray-lightest box box-small service'>      
            <img src='<?php echo INCLUDE_PATH . '/img/sposa_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Per La Sposa!</h3>                
            </div>
            <div class='modal_content'>                
                <span class='fechar'>&times;</span>   
                <p class='detail txt-center'>Um dia da noiva diferente e leve, onde
                    iremos proporcionar à você todo cuidado e tranquilidade que uma noiva precisa para se
                    preparar para o seu grande dia... Além de te deixar MARAVILHOSA!</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <ul class='disc'>
                    <li>Café da manhã, coffee break e almoço;</li>
                    <li>Massagem relaxante;</li>
                    <li>Banho de imersão terapêutica com cromoterapia;</li>
                    <li>Design de sobrancelhas;</li>
                    <li>Meditação, Reiki e Dinâmica terapêutica;</li>
                    <li>Penteado (opcional para mais um acompanhante);</li>
                    <li>Maquiagem (opcional para mais um acompanhante);</li>
                    <li>Teste da noiva (agendado com antecedência);</li>
                    <li>Making Off.</li>
                </ul>  
                <p><b>Duração:</b> 06 a 08 horas (aproximadamente).</p>                
            </div>                   
        </article>

        <article class='bg-gray-lightest box box-small service'>   
            <img src='<?php echo INCLUDE_PATH . '/img/speciale_poster.jpg'; ?>' />
            <div class='content'>
                <h3 class='font-300 txt-center'>Per La Sposa Speciale!</h3>               
            </div>
            <div class='modal_content'>                
                <span class='fechar'>&times;</span>  
                <p class='detail txt-center'>Toda noiva precisa de auxílio.
                    Foi pensando nisso que desenvolvemos esse programa exclusivo. Com acompanhamento
                    terapêutico, visagista e consultoria de imagem e estilo, você estará amparada em 
                    todas as fases que antecedem o seu grande dia, até o momento SIM</p>
                <p class='detail bg-gray-light'><b>Detalhes do Serviço</b></p>
                <p>As noivas poderão nos procurar a qualquer momento que anteceda seu casamento, porém, orientamos
                    que nos procure antes de suas principais escolhas estéticas (vestido, maquiagem, cabelo, etc.).</p>
                <p><b>Antes do Casamento</b></p>                    
                <ul class='disc'>
                    <li>Análise visagista e terapêutica para identificação
                        de perfil psicológico, comportamental e temperamental da noiva;</li>
                    <li>Sessões terapêuticas de acompanhamento (04 sessões);</li>
                    <li>Consulta com Consultora de Imagem e Estilo para escolha de modelo 
                        de vestido e sapato;</li>
                    <li>Teste da noiva (agendado com antecedência);</li>
                </ul>  
                <p><b>No Dia do Casamento</b></p>
                <ul class='disc'>
                    <li>Café da manhã, almoço coffee break;</li>
                    <li>Massagem relaxante;</li>
                    <li>Banho de imersão terapêutica com cromoterapia;</li>
                    <li>Meditação, Reiki e Dinâmica terapêutica;</li>
                    <li>Penteado (opcional para mais um acompanhante);</li>
                    <li>Maquiagem (opcional para mais um acompanhante);</li>
                    <li>Making Off.</li>
                </ul>                
            </div>
        </article>
        <div class='service_modal'></div>
        <p class='font-color-purple'><b>*Observação:</b> 
            Todos os processos são realizados sob supervisão terapêutica</p> 
    </div>

    <div class='clear'></div>
</section>
