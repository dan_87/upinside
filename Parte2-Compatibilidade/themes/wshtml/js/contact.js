$(document).ready(function()
{    
    $(document).on({
        ajaxStart: function() 
        {
            $('body').addClass('loading');    
        },
       
        ajaxStop: function() 
        {
            $('body').removeClass('loading'); 
        }    
    });
    
    $('form[name=contato]').submit(function()  
    {
        $('.msg').remove();
        var nome = $('input[name=nome]').val(); 
        var email = $('input[name=email]').val();
        var tel = $('input[name=tel]').val();
        var msg = $('textarea[name=msg]').val();
        var tipo = $('select[name=tipo]').val();
        var tipo_nome = $('select[name=tipo] option:selected').text();
        var data = 'nome=' + nome + '&email=' + email +
                   '&tel=' + tel + '&msg=' + msg + 
                   '&tipo=' + tipo + '&tipo_nome=' +tipo_nome ;

        $.ajax
        ({
            type: 'POST',
            url: 'mailer',
            data: data,
            success: function(data)
            {
                var retorno = JSON.parse(data);
                if(retorno.sta)
                {
                    $('#alerta').append("<span class='msg msg-success'>Mensagem enviada! Aguarde o retorno pelo seu email!</span>");                      
                }

                else
                {
                    $('#alerta').append("<span class='msg msg-error'>Mensagem não enviada! Verifique se todos os campos estão preenchidos corretamente!</span>");    
                }                
            },            
            error: function()
            {
                $('#alerta').append("<span class='msg msg-error'>Nem foi!</span>");    
            }
        });
        
        event.preventDefault();
    });
    
    $('input[name=reset]').click(function()
    {
        $('.msg').remove();
    });
});