$(document).ready(function()
{
    var modal = $('.service_modal');
    var conteudo = '';
    var fechar = $('.fechar');
    
    $('.service').click(function() 
    {
        conteudo = $(this).find('.modal_content').eq(0);
        $(this).addClass('active');
        $(conteudo).css('display', 'block');
        $(modal).css('display', 'block'); 
    });

    $(window).click(function(e) 
    {
        if (e.target.className === modal.attr('class') ||
                e.target.className === fechar.attr('class')) 
        {
            $(conteudo).css('display', 'none');    
            $(modal).css('display', 'none');
        }
    });
});