$(document).ready(function() 
{   
    $('.video_play').click(function()
    {       
        if($(this).hasClass('video_play'))
        {
            var video = $(this).next('img').attr('title');
            var diretorio = 'uploads/media/';
            $('html, body').animate({scrollTop: $('#main_video').offset().top}, 'slow');
            
            if(video == 'Conceito')
            {
                diretorio += 'main/';
            }         
            
            $('video').attr({src: diretorio + video + '.mp4', 
                                 autoplay: true});   
        }
    });
});

