$(document).ready(function() 
{ 
    var pagina = 1;
    var mostrar = $('.mostrar').val();
    $('#_' + pagina).addClass('ativa');    
    carregar_imagens(pagina, mostrar); 
    
    $(document).on({
        ajaxStart: function() 
        {
            $('body').addClass('loading');    
        },
       
        ajaxStop: function() 
        {
            $('body').removeClass('loading'); 
            $('#_' + pagina).addClass('ativa');
        }    
    });    
   
    function carregar_imagens(pagina, mostrar)
    {
        $.ajax(
        {
            url: 'inc_imagens',
            method: 'POST',
            data: 
            {
                pagina: pagina,
                mostrar: mostrar
            },
            
            success: function(data)
            {
                $('#imagens').html(data);           
            }            
        })
    }
    
    $(document).on('click', '.numero_pagina', function()
    {
       pagina = $(this).text();
       mostrar = $('.mostrar').val();
       carregar_imagens(pagina, mostrar);
    });
    
    $('.mostrar').change(function()
    {
       mostrar = $('.mostrar').val();
       carregar_imagens(1, mostrar);
    });    
    
    var modal = $('.service_modal');
    var conteudo = $('.modal_img');
    var pgw = $('.slider').pgwSlider();

    $(document).on('click', 'img', function() 
    {
        var img = $(this).attr('id');
        pgw.reload(
        {
            displayList: false,
            displayControls: true,
            autoSlide: false
        });
        
        pgw.displaySlide(img);
        $(conteudo).css('display', 'block');
        $(modal).css('display', 'block');
    });

    $(document).on('click', '.fechar', function() 
    {
        $(conteudo).css('display', 'none');    
        $(modal).css('display', 'none');
    });

    $(window).click(function(e) 
    {
        if (e.target.className == modal.attr('class')) 
        {
            $(conteudo).css('display', 'none');    
            $(modal).css('display', 'none');
        }
    });
});