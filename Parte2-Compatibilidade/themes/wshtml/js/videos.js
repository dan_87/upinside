$(document).ready(function()
{
    var pagina = 1;
    var mostrar = $('.mostrar').val();
    $('#_' + pagina).addClass('ativa');    
    carregar_videos(pagina, mostrar);
    
    $(document).on({
        ajaxStart: function()
        {
            $('body').addClass('loading');
        },

        ajaxStop: function()
        {
            $('body').removeClass('loading');
            $('#_' + pagina).addClass('ativa');
        }
    });
    
    function carregar_videos(pagina, mostrar)
    {
        $.ajax(
        {
            url: 'inc_videos',
            method: 'POST',
            data:
                    {
                        pagina: pagina,
                        mostrar: mostrar
                    },

            success: function (data)
            {
                $('#videos').html(data);
            }
        });
    }

    $(document).on('click', '.numero_pagina', function ()
    {
        pagina = $(this).text();
        mostrar = $('.mostrar').val();
        carregar_videos(pagina, mostrar);
    });

    $('.mostrar').change(function ()
    {
        mostrar = $('.mostrar').val();
        carregar_videos(1, mostrar);
    });

    var modal = $('.service_modal');
    var conteudo = $('.modal_video');
    var video = $(conteudo).find('video')[0];

    $(document).on('click', 'img', function ()
    {
        var id = $(this).attr('id');
        var vid = $(this).attr('alt');
        var thumb = $(this).attr('src');
        vid = (id == 1) ? 'uploads/media/main/' + vid + '.mp4' :
                'uploads/media/' + vid + '.mp4';

        $(video).attr(
                {
                    'src': vid,
                    'poster': thumb,
                    'autoplay': true
                });

        $(conteudo).css('display', 'block');
        $(modal).css('display', 'block');
    });

    $(document).on('click', '.fechar', function ()
    {
        if (!video.paused)
        {
            video.pause();
        }

        $(conteudo).css('display', 'none');
        $(modal).css('display', 'none');
    });

    $(window).click(function (e)
    {
        if (e.target.className === modal.attr('class'))
        {
            if (!video.paused)
            {
                video.pause();
            }

            $(conteudo).css('display', 'none');
            $(modal).css('display', 'none');
        }
    });
});