<section class='container main_bus' itemscope itemtype='https://schema.org/Organization'>
    <div class='content'>
        <header class='section-title'>
            <h2 class='font-large'>Sobre a <span itemprop='name'>Reflessione</span></h2>
            <p class='tagline'><?php echo $pg_desc; ?></p>
            <p>
                O desejo de criar um serviço único e capaz de possibilitar mudanças reais na vida das 
                pessoas deu vida a Reflessione, uma empresa criada pela Visagista <b>Celiani Zonta</b> e 
                pelo Terapeuta Transpessoal <b>Thyago Augusto</b>. 
                Através da análise visagista aliada a abordagem psicológica transpessoal e o uso da PNL, 
                é possível identificar o perfil comportamental, psicológico e temperamental de cada pessoa,
                possibilitando dar início a uma mudança única, extraindo de dentro para fora todas as 
                características que são necessárias para que essa adequação estética aconteça de forma 
                personalizada, com todo suporte e acolhimento necessários para receber essa nova visão de sí. 
                <span itemprop='description'>
                    A Reflessione é uma empresa para quem deseja mudança, seja ela em sua vida pessoal, 
                    profissional, sentimental ou qualquer outro momento em que se perceba a necessidade de 
                    readequação de imagem. </span>
            </p>
        </header>        

        <aside class='box box-medium'>
            <h3 class='title'>Redes Sociais</h3>
            <ul>
                <li>
                    <a itemprop='url' class='icon icon-facebook' title='Reflessione no Facebook' 
                       href='https://www.facebook.com/reflessione'>Facebook</a>                        
                </li>
                <li>
                    <a  itemprop='url' class='icon icon-instagram' title='Reflessione no Instagram' 
                        href='https://www.instagram.com/reflessione'>Instagram</a>                        
                </li>
                <li>
                    <a  itemprop='url' class='icon icon-youtube' title='Reflessione no Youtube' 
                        href='https://www.youtube.com/channel/UCwlZrR5lfgUHgrRYTyJJX3w'>YouTube</a>                        
                </li>
                <li>
                    <a  itemprop='url' class='icon icon-linkedin' title='Reflessione no Linkedin' 
                        href='https://br.linkedin.com/in/reflessione-9a74a1122'>LinkedIn</a>                        
                </li>
            </ul>
            <div class='clear'>
        </aside>           

        <article class='box box-medium'>
            <h3 class='title'>Contato</h3>
            <ul>
                <li>
                    <a itemprop='email' class='icon icon-mail' title='Envie um Email'
                       href='mailto:Reflessione <<?php echo $pg_email; ?>>'>
                        contato@reflessione.com.br
                    </a>
                </li>
                <li>
                    <p title='Reflessione pelo WhatsApp' class='icon icon-whatsapp'>                    
                        <span itemprop='telephone'>+55(11)98704-4444</span>
                    </p>
                </li>
                <li>
                    <p class='icon'><span class='icon-whatsapp font-zero'></span>
                        <span itemprop='telephone'>+55(11)95330-3620</span>
                    </p>
                </li>

                <li class='font-zero'><b>Site Oficial</b>
                    <a itemprop='sameAs' href='http://www.reflessione.esy.es'
                       title='Site Oficial de Reflessione'>reflessione.esy.es</a>
                </li>                    
            </ul>
        </article>

        <article class='box box-medium'>
            <h3 class='title'>Endereço</h3>
            <ul>
                <li>
                    <p itemscope itemtype='https://schema.org/PostalAddress'>
                        <span itemprop='streetAddress'>Rua das Palmeiras, 354 - Cj 01</span>  
                        <br>
                        <span itemprop='postalCode'>01226-010</span> | Santa Cecília
                        <br>
                        <span itemprop='addressLocality'>São Paulo</span> - 
                        <span itemprop='addressRegion'>SP</span>
                        <br>
                        <span itemprop='addressCountry'>Brasil</span>
                        <br>
                        A 500m.  das estações Marechal Deodoro e Santa Cecília.
                        <span itemscope itemtype='https://schema.org/GeoCoordinates'>
                            <meta itemprop='latitude' content='-23.536605'>
                            <meta itemprop='longitude' content='-46.652054'>
                        </span>
                    </p>
                </li>
            </ul>
        </article>            

        <div class='clear'></div>
    </div>  
</section>


<section class='container bg-purple-light'>         
    <div class='content'>
        <header class='section-title font-color-gray-light'>
            <h2>Conheça Nossa Equipe</h2>
            <p class='tagline'>Nós acreditamos no poder da autoestima!</p>
        </header>
    </div>

    <article class='container bg-green-light main_bus_person' itemscope itemtype='https://schema.org/Person'>
        <div class='content'>

            <div class='ft-left box box-four-last'>
                <img itemprop='image' class='round shadow' title='Thyago Augusto'
                     alt='Thyago Augusto' src='<?php echo HOME . '/uploads/img/thyago-portrait.jpg'; ?>'/>
            </div>


            <div class='ft-right box box-four'>
                <header class='article-title txt-justify'>
                    <h3>
                        <span itemprop='name'>Thyago Augusto</span>
                    </h3>
                    <p class='tagline'>
                        <span itemprop='jobTitle'>Terapeuta Transpessoal, Master em PNL 
                            e Reikiano</span>. <span itemprop='description'>Ele é a estrutura terapêutica da empresa, 
                            trazendo do consultório, toda a experiência necessária em 
                            cuidar de pessoas. Sendo assim o responsável por manter todos em sintonia!</span>
                    </p>
                    <p>
                        Em sua busca pessoal pelo autoconhecimento, percebeu que fazia parte do seu propósito 
                        de vida possibilitar às pessoas o encontro com o seu próprio caminho e o seu equilíbrio interno e externo. 
                        Desta forma, foi em busca de conhecimento através da psicologia, na abordagem integrativa transpessoal. 
                        Por ser uma ciência transdisciplinar, possibilitou-o a utilizar várias psicoterapias e técnicas aliadas, como
                        a programação neuro linguística PNL, o reiki, as mandalas, a meditação, arte terapia entre outras. 
                        Todo este conhecimento aplicado pode proporcionar o equilíbrio entre a mente, o corpo e a espiritualidade, 
                        a elevação da auto estima e o bem estar, auxiliando para que os objetivos e desejos pessoais possam ser 
                        traçados e alcançados. Identificando quais são os medos e crenças limitantes para que possam ser compreendidos
                        e resolvidos de uma maneira harmoniosa e respeitosa.Tudo isto é realizado com muita responsabilidade, dedicação 
                        e amor às pessoas e ao trabalho.
                    </p>
                </header>

                <div>
                    <ul>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow ta' target='_blank' title='Página Pessoal de Thyago'
                               href='http://www.thyagoaugusto.com.br'>Thyago</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow f' target='_blank' title='Thyago no Facebook'
                               href='https://www.facebook.com/thyago.augusto.31'>Facebook</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow t' target='_blank' title='Thyago no Twitter'
                               href='https://twitter.com/tnaugusto'>Twitter</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow l' target='_blank' title='Thyago no LinkedIn'
                               href='https://br.linkedin.com/in/thyago-augusto-472b36121'>LinkedIn</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow i' target='_blank' title='Thyago no Instagram'
                               href='https://www.instagram.com/thyagonaugusto/'>Instagram</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow y' target='_blank' title='Thyago no Youtube'
                               href='https://www.youtube.com/channel/UCq4aRazEE65OlxaPnqEPMoQ'>Youtube</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='email' class='shadow m' title='Enviar e-mail para Thyago'
                               href='mailto:Thyago Augusto <contato@thyagoaugusto.com.br>'>E-mail</a>
                        </li>
                    </ul>
                </div>
            </div>        

            <div class='clear'></div>
        </div>    
    </article>

    <article class='container bg-pink main_bus_person' itemscope itemtype='https://schema.org/Person'>
        <div class='content'>

            <div class='ft-right box box-four-last'>
                <img itemprop='image' class='round shadow' title='Celiani Zonta'
                     alt='Celiani Zonta' src='<?php echo HOME . '/uploads/img/celiani-portrait.jpg'; ?>'/>
            </div>

            <div class='ft-left box box-four'>
                <header class='article-title txt-justify'>
                    <h3>
                        <span itemprop='name'>Celiani Zonta</span>                    
                    </h3>
                    <p class='tagline'>
                        <span itemprop='description'>Ela é a responsável por toda adequação de imagem da empresa e tem 
                            paixão por cuidar e fazer a diferença na vida das pessoas!</span> 
                    </p>
                    <p class='txt-justify'>                   
                        <span itemprop='jobTitle'>Visagista, Hairstylist, Maquiadora e 
                            Design de Sobrancelhas</span>, formada pelas melhores escolas de São Paulo. Formada em visagismo
                        por Philip Hallawell, descobriu todo o lado terapêutico de sua profissão. Atendendo pessoas que 
                        buscam grandes transformações em suas vidas, adequando-as na parte de beleza e estética, 
                        com muita responsabilidade e amor pelo que faz.
                    </p>
                </header>

                <div>
                    <ul>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow ce' target='_blank' title='Página Pessoal de Celiani'
                               href='http://www.celizonta.com.br/'>Celiani</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow f' target='_blank' title='Celiani no Facebook'
                               href='https://www.facebook.com/celiani.zonta'>Facebook</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow t' target='_blank' title='Celiani no Twitter'
                               href='https://twitter.com/celi_pinup'>Twitter</a>
                        </li>
                        <li class='ds-inblock padding-bottomm'>
                            <a itemprop='url' class='shadow l' target='_blank' title='Celiani no LinkedIn'
                               href='https://br.linkedin.com/in/celi-zonta-82869a12/pt'>LinkedIn</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow i' target='_blank' title='Celiani no Instagram'
                               href='https://www.instagram.com/celizonta/'>Instagram</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='url' class='shadow y' target='_blank' title='Celiani no Youtube'
                               href='https://www.youtube.com/user/celiani1'>Youtube</a>
                        </li>
                        <li class='ds-inblock padding-bottom'>
                            <a itemprop='email' class='shadow m' title='Enviar e-mail para Celiani'
                               href='mailto:Celiani Zonta <contato@celizonta.com.br>'>E-mail</a>
                        </li>
                    </ul>
                </div>
            </div>        

            <div class='clear'></div>
        </div>    
    </article>

    <div class='container bg-gray-light'>
        <div class='content'>

            <article class='box box-medium txt-center main_bus_alt'>
                <img class='shadow round' alt='Stephanie Bergmann' title='Stephanie Bergmann'
                     src='<?php echo HOME . '/uploads/img/stephanie-portrait.jpg'; ?>'/>
                <header>
                    <h3>Stephanie Bergmann</h3>
                    <p class='tagline font-700'>Maquiadora e Educadora</p>
                </header>                   
                <p>Visagista, maquiadora e professora, a "Teph" 
                    trás toda sua experiência para ensinar auto maquiagem de uma maneira fácil e personalizada!</p>
            </article> 

            <article class='box box-medium txt-center main_bus_alt'>
                <img class='shadow round' alt='Fernanda Andrade' title='Fernanda Andrade'
                     src='<?php echo HOME . '/uploads/img/fernanda-portrait.jpg'; ?>'/>
                <header>
                    <h3>Fernanda Andrade</h3>
                    <p class='tagline font-700'>Consultora de Imagem e Orientadora</p>
                </header>                    
                <p>Nossa Personal Stylist, a "Fê" orienta de uma maneira 
                    graciosa como ter um estilo único, autêntico e adequado ao seu ritmo de vida.</p>
            </article> 

            <article class='box box-medium txt-center main_bus_alt'>
                <img class='shadow round' alt='Natália Grotto' title='Natália Grotto'
                     src='<?php echo HOME . '/uploads/img/natalia-portrait.jpg'; ?>'/>                    
                <header>
                    <h3>Natália Grotto</h3>
                    <p class='tagline font-700'>Fotógrafa e Encantadora</p>
                </header>
                <p>Fotógrafa apaixonada pelo universo feminino,
                    a "Nathi" é especialista nos mais diversos tipos de ensaios femininos!</p>
            </article> 

            <div class='clear'></div>
        </div>
    </div>
    <div class='clear'></div>  
</section>