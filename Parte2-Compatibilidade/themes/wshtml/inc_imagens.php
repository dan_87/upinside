<?php

//Tratamento do POST
$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$pagina = isset($post['pagina']) ? $post['pagina'] : 1;
$mostrar = isset($post['mostrar']) ? $post['mostrar'] : 5;

//Declaração de variáveis
$x = 0;
$total = 0;
$datas = array();
$inicia = ($pagina - 1) * $mostrar;
$termina = ($inicia + $mostrar);

//Diretório de imagens upadas
$diretorio_img_js = '../../uploads/img/galeria/';
$diretorio_img_g = './uploads/img/galeria/';

//Lê todos os arquivos dentro do diretório e
//os coloca dentro de um array por data
foreach (scandir($diretorio_img_js) as $arquivo):
    if (is_file($diretorio_img_js . $arquivo) && $arquivo != 'Thumbs.db'):
        $total++;
        $data = date('Y-m-d H:i:s', filectime($diretorio_img_js . $arquivo));
        $data = new DateTime($data);
        $img_ordenado_g[$arquivo] = verificaData($data, $datas);
        $datas[] = $img_ordenado_g[$arquivo];
    endif;
endforeach;

$paginas = ceil($total / $mostrar);

//Ordena o array por data decrescente
arsort($img_ordenado_g, SORT_STRING);

//Obtém apenas os elementos da página clicada
$img_ordenado_g = array_slice($img_ordenado_g, $inicia, $mostrar);

//Cria uma página com as imagens de acordo com o nome do arquivo
$texto_inc_imagens = "<h3 class='font-zero'>Lista de Imagens</h3>";
foreach ($img_ordenado_g as $key => $value):
    $x++;
    $id = $x + $inicia;
    $alt = explode('.', $key);
    $src = $diretorio_img_g . $key;
    $texto_inc_imagens .= "<article class='box box-tiny'>
                    <img src='$src' alt='$alt[0]'
                        title='$alt[0]' id='$id' class='radius'>
                    <h4 class='img_title txt-center'>$alt[0]</h4>   
                </article>";
    if ($x % 5 == 0):
        $texto_inc_imagens .= "<div class='clear'></div>";
    endif;
endforeach;

$texto_inc_imagens .= "<div class='clear'></div><ul class='paginacao'>";

for ($x = 1; $x <= $paginas; $x++):
    $texto_inc_imagens .= "<li class='numero_pagina' id='_$x'>$x</li>";
endfor;

$texto_inc_imagens .= '</ul>';
echo utf8_encode($texto_inc_imagens);

function verificaData($data, $array) {
    $dataf = $data->format('d/m/Y H:i:s');
    if (in_array($dataf, $array)):
        $data->modify('-1 second');
        verificaData($data, $array);
    endif;

    $dataf = $data->format('d/m/Y H:i:s');
    return $dataf;
}
