<article class='container'>            
    <div class='content'>
        <header class='section-title' id='main_video'>
            <h2 class='font-large'>Conheça a Reflessione</h2>
            <p class='tagline'>Veja na prática como funciona o nosso atendimento!
                &nbsp;<span class='icon icon-arrowdown'></span></p>
        </header> 

        <!-- VÍDEO DE APRESENTAÇÃO -->        

        <video class='main_video vga vga-large radius' controls preload='none' 
               onclick='(!this.paused ? this.pause() : this.play());'
               poster='uploads/media/main/main.jpg'
               src='uploads/media/main/Conceito.mp4'>
        </video>

        <!-- VÍDEO LOCAL

        <iframe width='700' height='400' class='main_video vga vga-large radius' 
                src='https://www.youtube.com/embed/sAQtK_iWfdw' frameborder='0'
                allowfullscreen></iframe>
                        
        /VÍDEO LOCAL -->

        <!-- /VÍDEO DE APRESENTAÇÃO -->

        <div class='clear'></div>
    </div> 

    <footer class='bg-green-light '>                
        <section class='content main_media font-small'> 
            <header>
                <h3 class='section-title'>Assista também...</h3>
            </header>

            <!-- VIDEOS -->     
            <?php require('inc/main_videos.php'); ?>
            <script src='<?php echo REQUIRE_PATH . '/js/main_video.js'; ?>'></script>            
            <!-- /VÍDEOS -->

            <div class='clear'></div>
            <div class='more' onclick="document.location.href = 'videos'">Veja mais...</div>
            <div class='clear'></div>
        </section>

    </footer>
</article> 

<!-- SERVIÇOS -->

<section class='container bg-purple-light'>
    <div class='content'>
        <header class='section-title font-color-gray-light'>
            <h2>Aprecie os Nossos Serviços</h2>
            <p class='tagline'>Somos especialistas em adequar sua imagem e 
                extrair sua verdadeira beleza! <br/>Você nunca viu nada igual! 
                Programas especiais para noivas e gestantes!</p>
        </header>
    </div>

    <div class='container bg-default'>
        <div class='content'>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Iniziale!' alt='Iniziale!' 
                     src='<?php echo REQUIRE_PATH . '/img/iniziale.jpg'; ?>'/>
                <h3>Iniziale!</h3>
                <p class='tagline font-small'>O primeiro passo para a mudança 
                    que você deseja!</p>
            </article>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Essenziale!' alt='Essenziale!' 
                     src='<?php echo REQUIRE_PATH . '/img/essenziale.jpg'; ?>'/>
                <h3>Essenziale!</h3>
                <p class='tagline font-small'>Aprenda a viver a mudança todos 
                    os dias de sua vida!</p>
            </article>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Sensazionale!' alt='Sensazionale!'
                     src='<?php echo REQUIRE_PATH . '/img/sensazionale.jpg'; ?>'/>
                <h3>Sensazionale!</h3>
                <p class='tagline font-small'>Redescubra-se e registre esse 
                    momento único!</p>
            </article>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Fenomenale!' alt='Fenomenale!' 
                     src='<?php echo REQUIRE_PATH . '/img/fenomenale.jpg'; ?>'/>
                <h3>Fenomenale!</h3>
                <p class='tagline font-small'>Vivendo a mudança da cabeça aos 
                    pés!</p>
            </article>

            <div class='clear'></div>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Tranquillità!' alt='Tranquillità!' 
                     src='<?php echo REQUIRE_PATH . '/img/tranquillita.jpg'; ?>'/>
                <h3>Tranquillità!</h3>
                <p class='tagline font-small'>O seu momento para relaxar e se 
                    cuidar!</p>
            </article>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Maternità!' alt='Maternità!' 
                     src='<?php echo REQUIRE_PATH . '/img/maternita.jpg'; ?>'/>
                <h3>Maternità!</h3>
                <p class='tagline font-small'>Um dia de cuidados especiais 
                    para a futura mamãe!</p>
            </article>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Per La Sposa!' alt='Per La Sposa!'
                     src='<?php echo REQUIRE_PATH . '/img/sposa.jpg'; ?>'/>
                <h3>Per La Sposa!</h3>
                <p class='tagline font-small'>Uma programação encantadora 
                    para um dia tão especial!</p>
            </article>

            <article class='box box-small main_service txt-center'>
                <img class='round' title='Per La Sposa Speciale!' 
                     alt='Per La Sposa Speciale!' 
                     src='<?php echo REQUIRE_PATH . '/img/speciale.jpg'; ?>'/>
                <h3 class='last'>Per La Sposa Speciale!</h3>
                <p class='tagline font-small'>Para noivas que buscam tranquilidade
                    e segurança em seu grande dia!</p>                
            </article>       
            
            <div class='clear'></div>
            <div class='more' onclick="document.location.href = 'servicos'">Veja mais...</div>
            <div class='clear'></div>
        </div>
    </div>

</section>

<!-- /SERVIÇOS -->

<!-- GALERIA DE FOTOS -->
<section class='container bg-blue-light'>
    <div class='content'>
        <header class='section-title'>
            <h2>Galeria de Imagens</h2>
            <p class='tagline'>Veja também nossa galeria de imagens!</p>                    
        </header>

        <div id='pgwSlider'>       
            <ul class='slider'>
                <?php require('inc/main_imagens.php'); ?>
            </ul>
        </div>

        <div class='clear'></div>
        <div class='more' onclick="document.location.href = 'imagens'">Veja mais...</div>
        <div class='clear'></div>
    </div>
</section>
<script src='<?php echo REQUIRE_PATH . '/js/pgw.js'; ?>'></script>
<script src='<?php echo REQUIRE_PATH . '/js/main_slider.js'; ?>'></script>
<!-- /GALERIA DE FOTOS -->
