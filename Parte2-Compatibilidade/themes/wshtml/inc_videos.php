<?php
//Tratamento do POST
$post = filter_input_array(INPUT_POST, FILTER_DEFAULT);
$pagina = isset($post['pagina']) ? $post['pagina'] : 1;
$mostrar = isset($post['mostrar']) ? $post['mostrar'] : 5;

//Declaração de variáveis
$x = 0;
$total = 0;
$datas_modal = array();
$inicia = ($pagina - 1) * $mostrar;
$termina = ($inicia + $mostrar);

//Diretório de imagens upadas
$diretorio_video_js = '../../uploads/media/';
$diretorio_video_g = './uploads/media/';

//Lê todos os arquivos dentro do diretório e
//os coloca dentro de um array por data
foreach(scandir($diretorio_video_js) as $arquivo):
    if(is_file($diretorio_video_js . $arquivo) && $arquivo != 'Thumbs.db'):
        $total++;
        $data = date('Y-m-d H:i:s', filectime($diretorio_video_js . $arquivo));
        $data = new DateTime($data);       
        $video_ordenado_g[$arquivo] = verificaData($data, $datas_modal);
        $datas_modal[] = $video_ordenado_g[$arquivo];
    endif;
endforeach;

//Obtém apenas os elementos da página clicada
$paginas = ceil($total / $mostrar);

if($pagina == 1):
    $mostrar -= 1;
endif;

//Ordena o array por data decrescente
arsort($video_ordenado_g, SORT_STRING);

//Obtém apenas os elementos da página clicada
$video_ordenado_g = array_slice($video_ordenado_g, $inicia, $mostrar);

$texto_inc_videos = "<h3 class='font-zero'>Lista de Vídeos</h3>";
    
foreach($video_ordenado_g as $key=>$value):
    if($pagina == 1 && $x == 0):
        $x++;
        $texto_inc_videos .=  "<article class='box box-tiny'>
                                    <div class='thumb'>
                                      <div class='video_play radius'></div>
                                      <img title='Conceito' alt='Conceito' class='radius'
                                           id='$x' src='$diretorio_video_g/main/Conceito.jpg'/>
                                    </div>
                                    <h4 class='img_title txt-center'>Conceito</h4>
                                </article>" ;
    endif;
    
    $x++;       
    $id = $x + $inicia;
    $alt = explode('.' , $key);
    $src = $diretorio_video_g . 'thumb/' . $alt[0] . 'jpg';
    $thumb_alt = $diretorio_video_js . 'thumb/' . $alt[0] . '.jpg';
    $thumb = (file_exists($thumb_alt) ?  $diretorio_video_g . $alt[0]
             . '.jpg' : $diretorio_video_g . 'thumb/thumb.png');
        $texto_inc_videos .=  "<article class='box box-tiny'>
                                    <div class='thumb'>
                                        <div class='video_play radius'></div>
                                        <img title='" . $alt[0] . "'
                                        alt='$alt[0]' src='$thumb' id='$id' class='radius'/>
                                    </div>
                                    <h4 class='img_title txt-center'>$alt[0]</h4>
                                </article>";
    if($x % 5 == 0):
        $texto_inc_videos .= "<div class='clear'></div>";
    endif;

endforeach;

$texto_inc_videos .= "<div class='clear'></div><ul class='paginacao'>";

for($x=1; $x<=$paginas; $x++):
    $texto_inc_videos .= "<li class='numero_pagina' id='_$x'>$x</li>";
endfor;

$texto_inc_videos .= '</ul>';
echo utf8_encode($texto_inc_videos);

function verificaData($data, $array)
{
    $dataf = $data->format('d/m/Y H:i:s');
    if(in_array($dataf, $array)):
        $data->modify('-1 second');
        verificaData($data, $array);
    endif;
    
    $dataf = $data->format('d/m/Y H:i:s');
    return $dataf;
}