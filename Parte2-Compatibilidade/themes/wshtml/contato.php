<div class='modal'></div>   
<section class='container bg-gray-light'>
    <div class='content bg-map'>
        <header class='section-title'>
            <h2 class='font-large'>Formas de <?php echo $pg_title; ?></h2>
            <p class='tagline'><?php echo $pg_desc; ?></p>
        </header>

        <aside class='ft-right box-three-last'>
            <h3 class='form-subtitle txt-center'>
                <span class='icon icon-phone icon-subtitle'></span>
                Fale Conosco
            </h3>
            <ul>
                <li>
                    <b>Por E-mail:</b>
                    <a title='Envie um e-mail com sua sugestão!'
                       href='mailto:Reflessione <contato@reflessione.com.br>'>
                        contato@reflessione.com.br
                    </a>
                </li>
                <li><b>Pelo Facebook:</b>
                    <a title='Pelo Facebook' target='_blank' rel='nofollow'
                       href='http://www.facebook.com/reflessione'>
                        facebook.com/reflessione
                    </a>
                </li>
                <li><b>Por Telefone:</b>+55(11)98704-4444 | +55(11)95330-3620 </li>
            </ul>

            <p itemscope itemtype='https://schema.org/PostalAddress'>
                <b>Endereço:</b>
                <span itemprop='streetAddress'>Rua das Palmeiras, 354 - Cj 01</span>  
                <br>
                <span itemprop='postalCode'>01226-010</span> | Santa Cecília
                <br>
                <span itemprop='addressLocality'>São Paulo</span> - 
                <span itemprop='addressRegion'>SP</span>
                <br>
                <span itemprop='addressCountry'>Brasil</span>
                A 500m.  das estações Marechal Deodoro e Sta. Cecília.
                <span itemscope itemtype='https://schema.org/GeoCoordinates'>
                    <meta itemprop='latitude' content='-23.536605'>
                    <meta itemprop='longitude' content='-46.652054'>
                </span>
            </p> 

            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.8861930081207!2d-46.65427718540355!3d-23.536595366562373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce583f9480245b%3A0xe17d4f64b04a348!2sR.+das+Palmeiras%2C+354+-+Vila+Buarque%2C+S%C3%A3o+Paulo+-+SP%2C+01226-010!5e0!3m2!1spt-BR!2sbr!4v1484590434401&z=1"
                    class='iframe_contato radius' allowfullscreen></iframe>
        </aside>

        <article class='ft-left box-three'>
            <form action='' method='post' name='contato' autocomplete="off">
                <header>
                    <h3 class='form-subtitle txt-center'>
                        <span class='icon icon-contact icon-subtitle'></span>
                        Envie uma mensagem
                    </h3>
                </header>

                <label>
                    <span class='form-label'>Mensagem:</span>
                    <select name='tipo'>
                        <option value='0'>Agradecimento</option> 
                        <option value='1' selected>Dúvida</option>
                        <option value='2'>Sugestão</option>
                    </select>
                </label>

                <label>
                    <span class='form-label'>Nome:</span>
                    <input type='text' name='nome' title='Digite seu nome' 
                           required placeholder='Informe seu nome'/>
                </label>

                <label>
                    <span class='form-label'>E-mail:</span>
                    <input type='email' name='email' pattern='[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$' 
                           title='Digite um e-mail válido' required placeholder='Digite seu e-mail para contato'/>
                </label>

                <label>
                    <span class='form-label'>Telefone:</span>
                    <input type='tel' name='tel' pattern='[0-9]+$' required
                           title='Digite um telefone válido' maxlength='11' 
                           placeholder='Deixe seu telefone para contato (apenas números)'/>
                </label>                    

                <label>
                    <span class='form-label'>Mensagem:</span>
                    <textarea name='msg' rows='6' id='msg' title='Deixe sua mensagem'
                              required maxlength='600' placeholder='Descreva sua mensagem'></textarea>
                </label>

                <div id='alerta'></div>

                <input type='submit' id='submit' class='btn btn-green radius' 
                       name='submit' value='Enviar'/>
                <input type='reset' name='reset' class='btn btn-blue radius' value='Limpar'/>

            </form>
        </article>

        <div class='clear'></div> 
    </div>
</section>
<script src='<?php echo REQUIRE_PATH . '/js/contact.js'; ?>'></script>
