<?php
// CONSTANTES DE DIRETÓRIOS

define('HOME', 'http://localhost/UpInside/Parte2-Compatibilidade');
define('THEME', 'wshtml');
define('INCLUDE_PATH', HOME . '/themes/' . THEME);
define('REQUIRE_PATH', 'themes/' . THEME);
define('IMG', INCLUDE_PATH . '/img/');

// URL AMIGÁVEL

$getUrl = strip_tags(trim(filter_input(INPUT_GET, 'url', FILTER_DEFAULT)));
$setUrl = (empty($getUrl) ? 'index' : $getUrl);
$url = explode('/', $setUrl);

// AUTENTICAÇÃO DAS REDES SOCIAIS

$pg_email = 'contato@reflessione.com.br';
$pg_aut = 'https://plus.google.com/u/0/106117655623534728856/posts';
$pg_fb_id = '1293890413998823';
$pg_fb_tok = 'access_token=1293890413998823|_wlbyqWRfgZHik2HG0mo19qrx9Y';

// VARIÁVEIS DE METADADOS DO HTML

$descricao['home'] = 'O desejo de criar um serviço único e capaz de possibilitar mudanças 
            reais na vida das pessoas deu vida a Reflessione, uma empresa criada para 
            quem deseja mudança, seja ela em sua vida pessoal, profissional, sentimental
            ou qualquer outro momento em que se perceba a necessidade de readequação de imagem. 
            Porque nós acreditamos no poder da auto estima!';
$descricao['sobre'] = 'Conheça a nossa empresa!';
$descricao['contato'] = 'Deixe sua mensagem, ou contate-nos!';
$descricao['servicos'] = 'Saiba como funcionam os nossos atendimentos!';
$descricao['imagens'] = 'Veja nossa galeria de imagens!';
$descricao['videos'] = 'Veja nossa galeria de vídeos!';
$descricao['404'] = "Não foi possível localizar o conteúdo <b>$setUrl</b> no nosso portal!";

switch($url[0]):
    case 'index':
        $pg_title = 'Home';
        $pg_desc = $descricao['home'];
        $pg_img = IMG . 'home.jpg';
        $pg_url = HOME;
        break;
    case 'sobre':
        $pg_title = 'Sobre';
        $pg_desc = $descricao['sobre'];
        $pg_img = IMG . 'sobre.jpg';
        $pg_url = HOME . '/sobre';
        break;
    case 'contato':
        $pg_title = 'Contato';
        $pg_desc = $descricao['contato'];
        $pg_img = IMG . 'contato.jpg';
        $pg_url = HOME . '/contato';
        break;
    case 'servicos':
        $pg_title = 'Serviços';
        $pg_desc = $descricao['servicos'];
        $pg_img = IMG . 'servicos.jpg';
        $pg_url = HOME . '/servicos';   
        break;
    case 'imagens':
        $pg_title = 'Imagens';
        $pg_desc = $descricao['imagens'];
        $pg_img = IMG . 'imagens.jpg';
        $pg_url = HOME . '/imagens';   
        break;
    case 'videos':
        $pg_title = 'Vídeos';
        $pg_desc = $descricao['videos'];
        $pg_img = IMG . 'videos.jpg';
        $pg_url = HOME . '/videos';   
        break;
    default:
        $pg_title = '404';
        $pg_desc = $descricao['404'];
        $pg_img = IMG . '404.jpg';
        $pg_url = HOME . '/404';
        break;        
endswitch;

$pg_header = 'Reflessione | ' . $pg_title;

function ping()
{
    $ping = array();
    $ping['google'] = 'https://www.google.com.br/webmasters/tools/ping?sitemap=' . HOME . '/sitemap.xml';
    $ping['bing'] = 'https://www.bing.com/webmaster/ping.aspx?siteMap=' . HOME . '/sitemap.xml';
    
    foreach($ping as $ping_url):
        $curl = curl_init($ping_url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($curl);
        curl_close($curl);
    endforeach;
}

if(!file_exists('sitemap.xml.gz')):
    $gz = gzopen('sitemap.xml.gz', 'w9');
    $gmap = file_get_contents('sitemap.xml');
    gzwrite($gz, $gmap);
    gzclose($gz);
    ping();
endif;


