<?php
require('_app/config.inc.php');
?>
<!DOCTYPE html>
<html lang='pt-BR' itemscope itemtype='https://schema.org/WebSite'>
    <head>
        <meta charset='UTF-8'/>

        <!-- ATIVAR MECANISMOS DE BUSCA -->

        <meta name='description' content='<?php echo $pg_desc; ?>'/>
        <meta name='robots' content='index, follow'/>

        <!-- /ATIVAR MECANISMOS DE BUSCA -->

        <!-- RELACIONAR SITE ÀS REDES SOCIAIS -->

        <link rel='author' href='<?php echo $pg_aut; ?>'/>
        <link rel='publisher' href='<?php echo $pg_aut; ?>'/>
        <link rel='canonical' href='<?php echo $pg_url; ?>'/>  
        <link rel='alternate' type='application/rss+xml' 
              href='<?php echo HOME . '/rss.xml'; ?>'/>

        <meta itemprop='name' content='<?php echo $pg_header; ?>'/>
        <meta itemprop='description' content='<?php echo $pg_desc; ?>'/>
        <meta itemprop='image' content='<?php echo $pg_img; ?>'/>
        <meta itemprop='url' content='<?php echo $pg_url; ?>'/>        

        <meta property='og:type' content='website'/>
        <meta property='og:title' content='<?php echo $pg_header; ?>'/>
        <meta property='og:description' content='<?php echo $pg_desc; ?>'/>
        <meta property='og:image' content='<?php echo $pg_img; ?>'/>
        <meta property='og:url' content='<?php echo $pg_url; ?>'/>   
        <meta property='fb:app_id' content='<?php echo $pg_fb_id; ?>'/>

        <!-- /RELACIONAR PÁGINAS ÀS REDES SOCIAIS -->        

        <title> <?php echo $pg_header; ?> </title>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'
                integrity='sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8='
        crossorigin='anonymous'></script>

        <!--[if lt IE 9]>
            <script src='https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js'>
            </script>
        <![endif]-->        

        <link rel='shortcut icon' href='<?php echo INCLUDE_PATH . '/css/boot/icons/reflessione.png'; ?>'/>
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.css'/>
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700'/>
        <link rel='stylesheet' href='<?php echo INCLUDE_PATH . '/css/boot.css'; ?>'/>       
        <link rel='stylesheet' href='<?php echo INCLUDE_PATH . '/css/style.css'; ?>'/>
        <link rel='stylesheet' href='<?php echo INCLUDE_PATH . '/css/pgw.css'; ?>'/>
    </head>
    <body>

        <?php require(REQUIRE_PATH . '/inc/analyticstracking.php'); ?>

        <!-- HEADER -->

        <header class='container'>            
            <div class='content padding-bottom-none'>
                <header>
                    <h1 class='main_logo font-zero'>
                        <a title='Home' href='<?php echo HOME; ?>' class='radius'>
                            <?php echo $pg_header; ?>
                        </a>
                    </h1>
                </header>

                <!-- MENU SUPERIOR -->

                <ul class='main_nav ft-right'>
                    <?php
                    require REQUIRE_PATH . '/inc/main_nav.php';
                    ?>
                </ul>

                <!-- /MENU SUPERIOR -->

                <div class='clear'></div>
            </div>            
        </header>

        <!-- /HEADER -->

        <!-- CONTEÚDO PRINCIPAL -->

        <?php
        $url[1] = (empty($url[1]) ? null : $url[1]);
        $required1 = REQUIRE_PATH . '/' . $url[0] . '.php';
        $required2 = REQUIRE_PATH . '/' . $url[0] . '/' . $url[1] . '.php';
        $error = REQUIRE_PATH . '/404.php';

        if (file_exists($required1)):
            require $required1;
        elseif (file_exists($required2)):
            require $required2;
        else:
            require $error;
        endif;
        ?>
        
        <div>
            <?php require REQUIRE_PATH . '/inc/social.php'; ?>
        </div>

        <!-- /CONTEÚDO PRINCIPAL -->       

        <!-- COMPLEMENTO CONVERSÃO -->        

        <div class='container'>
            <div class='content content-page txt-center font-medium 
                 font-300 font-color-purple'>
                Reflessione, beleza com responsabilidade terapêutica!
                <div class='clear'></div>
            </div>
        </div>

        <!-- /COMPLEMENTO CONVERSÃO -->  

        <!-- FOOTER -->

        <footer class='container bg-gray'>            
            <section class='main_footer content'>    
                <header>
                    <h2 class='font-zero'>Sobre a empresa</h2>
                </header>

                <!-- REDES SOCIAIS -->

                <article class='box box-medium'>
                    <header>
                        <h3 class='title font-small-b txt-center'>Redes Sociais</h3>
                    </header>

                    <ul>
                        <li>
                            <a title='Facebook' target='_blank' rel='nofollow'
                               href='http://facebook.com/reflessione'
                               class='icon icon-facebook'>Facebook</a>
                        </li>
                        <li>
                            <a title='Instagram' target='_blank' rel='nofollow'
                               href='https://instagram.com/reflessione'
                               class='icon icon-instagram'>Instagram</a>
                        </li>
                        <li>
                            <a title='LinkedIn' target='_blank' rel='nofollow'
                               href='https://br.linkedin.com/in/reflessione-9a74a1122'
                               class='icon icon-linkedin'>LinkedIn</a>
                        </li>   
                        <li>
                            <a title='YouTube' target='_blank' rel='nofollow'
                               href='https://youtube.com/channel/UCwlZrR5lfgUHgrRYTyJJX3w'
                               class='icon icon-youtube'>YouTube</a>
                        </li>                        
                    </ul>  
                </article>

                <!-- /REDES SOCIAIS -->

                <!-- ENDEREÇO FÍSICO -->

                <article class='box box-medium'>
                    <header>
                        <h3 class='title font-small-b txt-center'>Contato</h3>
                    </header>

                    <ul>
                        <li>
                            <a title='Envie um Email' class='icon icon-mail'
                               href='mailto:contato@reflessione.com.br'>
                                Envie um Email
                            </a>                    
                        </li>
                        <li>
                            <p title='Reflessione pelo WhatsApp' class='icon icon-whatsapp'>+55(11)98704-4444</p>
                        </li>
                        <li>
                            <p class='icon'><span class='icon-whatsapp font-zero'></span>
                                +55(11)95330-3620
                            </p>                                
                        </li>
                    </ul>                    
                </article>

                <!-- /ENDEREÇO FÍSICO -->

                <!-- GEOLOCALIZAÇÃO -->                

                <aside class='box box-medium'>
                    <header>
                        <h3 class='title font-small-b txt-center'>Localização</h3>
                    </header>

                    <iframe src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3657.8861930081207!2d-46.65427718540355!3d-23.536595366562373!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce583f9480245b%3A0xe17d4f64b04a348!2sR.+das+Palmeiras%2C+354+-+Vila+Buarque%2C+S%C3%A3o+Paulo+-+SP%2C+01226-010!5e0!3m2!1spt-BR!2sbr!4v1484590434401&z=1'
                            class='iframe_footer radius' allowfullscreen></iframe>
                </aside>

                <!-- /GEOLOCALIZAÇÃO -->     

                <div class='clear'></div>
            </section>  

            <!-- DIREITOS -->

            <div class='container rights bg-purple'>
                <div class='content'>
                    <p class='txt-center font-small'>COPYRIGHT &COPY; <?php echo date('Y'); ?> - 
                        Daniel Freitas - Todos os direitos reservados!</p>
                </div>
            </div>

            <!-- /DIREITOS -->

        </footer>

        <!-- /FOOTER -->
    </body>
</html>
