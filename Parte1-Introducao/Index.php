<!DOCTYPE html>
<html lang='pt-br'>
    <head>
        <meta charset='UTF-8'/>
        <meta name='robots' content='index, follow'/>
        <title> Início </title>
        <link rel='shortcut icon' href='css/flavicon.png'/>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js'></script>
        <script src='https://code.jquery.com/jquery-3.1.1.min.js'
                integrity='sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8='
                crossorigin='anonymous'></script>
        <script>function teste(texto)
            {
                document.getElementById('teste').innerHTML += '<p>' + texto + '</p>';
            }
            
            window.onload = function()
            {
                if(Modernizr.localstorage)
                {
                    teste('LocalStorage habilitado!');
                }
                
                if(Modernizr.geolocation)
                {
                    teste('GeoLocation habilitado!');
                }              
            };
            
            function check()
            {
                if(Modernizr.inputtypes.date)
                {
                    teste('Date habilitado');
                    document.getElementById('data').setAttribute('type', 'date');
                }
                
                else
                {
                    teste('<b>Date não pode ser habilitado!</b>');
                    $(function(){
                        var i = 0;
                        $('#data').attr('maxlength', '10');
                        $('#data').keyup(function(){
                            i++;
                            if(i % 2 === 0 && i < 6)
                            {
                                $(this).val($(this).val() + '/');
                            }
                        });
                    });
                }   
            };               
        </script>
    </head>  
    <body>
        <div id='teste'></div>
        <input type='text' name='data' onfocus='check()' id='data' placeholder='digite uma data'/> 
    </body>
</html>
